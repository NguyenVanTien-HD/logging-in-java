# **`Logging là gì?`**

Java cho phép chúng ta tạo và nắm bắt các thông báo và file nhật ký thông qua trình ghi nhật ký(Logging).

Trong Java, việc ghi nhật ký(Logging) được sử dụng trong các framework và API

# **`Ghi log thế nào cho hiệu quả?`**

`1. Format log`

Để việc sử dụng log đạt hiệu quả, trong mỗi team hoặc mỗi công ty cần có một chuẩn đặt log chúng để dễ dàng sử dụng sau khi thu thập log về chung một hệ thống lưu trữ.

Một cụm log nên chứa các thông tin sau:

Datetime: thời gian ghi log, tính đến milisecond, định dạng nên dùng: yyyy-MM-dd HH:mm:ss,SSS.
Ví dụ: 2020-09-17 09:50:55,901. Hoặc có thể sử dụng ISO local datetime (có thêm timezone)

Log Level: Phân loại theo từng mục đích sử dụng, tuân theo quy định chung của từng công ty. Việc phân chia log-level sẽ giúp cho việc giám sát/bảo trì phần mềm thuận tiện hơn. Các log-level thường gặp: DEBUG, INFO, WARN, ERROR, CRITICAL/FATAL

Tên component/module/function phát sinh ra log, nên đưa cả dòng phát sinh ra lỗi (nếu có thể phát hiện ra được trong cảnh báo lỗi)

Log content: Nội dung lỗi muốn ghi vào file log.

Request id: Id của session/phiên làm việc/request thực hiện các action để sinh ra log. Nếu có trường dữ liệu này, đến khi kiểm tra các event của session ở file log, chỉ cần thực hiện CTRL + F (nếu mở log file bằng editor) hoặc cat...| grep .... (nếu xem log từ màn hình terminal)

Parameter input: Thông tin dữ liệu đầu vào request đến.

Result output: Thông tin dữu liệu đầu ra do hệ thống trả về cho request đến
Ví dụ: Một kiểu format log như sau:

`Datetime, Log Level, Component [code line] [Class/File name] [method/function...] Log Content [parameter input] [parameter output]
`

`2. Phân loại log-level`

DEBUG:
Log dùng để debug (rất nhiều và hỗn loạn). Mục đích sử dụng thường là dành cho developer.
Thông thường là đặt log level này enable trên môi trường LOCAL/DEVELOPING và disable ở PRODUCTION do số lượng dữ liệu log sẽ rất nhiều. Tuy nhiên đôi khi cũng phải enable lại level log này trên PRODUCTION để thực hiện ghi log và phát hiện ra các lỗi chỉ xảy ra trên PRODUCTION (Không phát sinh trên local).

![img_1.png](img_1.png)
![img_2.png](img_2.png)

Ví dụ: Hệ thống dữ liệu trên PRODUCTION thường lớn hơn nhiều so với DEVELOPING nên số lượng trường hợp xảy ra khi áp dữ liệu vào phần mềm cũng nhiều hơn. Các test-case có thể chưa bao quát được hết các lỗi --> Việc đặt debug có thể giúp developer/tester có thể giả lập lại được chính xác toàn bộ thao tác, workflow của cụm dữ liệu khi đi vào/đi ra khỏi phần mềm.

INFO:
Log mang tính chất cung cấp thông tin, sự kiện quan trọng khi ứng dụng hoạt đông, không nên mang ý nghĩa chỉ định lỗi.

![img.png](img.png)
Ví dụ: Hành vi đăng nhâp vào hệ thống bởi tài khoản nào, version của phần mềm, ...

WARN (WARNING):
Log lại các lỗi tiềm ẩn, có thể gây ra lỗi nhưng chưa ảnh hưởng đến trải nghiệm của người dùng.
Ví dụ: Khách hàng cố tình nhấn nhiều lần vào button thanh toán trong khi người dùng không đủ điều kiện để thanh toán, các warning sẽ được log lại khi khách hàng "cố gắng" làm sai với hướng dẫn.

ERROR:
Log lại các event trên hệ thống đã gây ra lỗi tương đối lớn, có thể gây ảnh hưởng đến trải nghiệm của người dùng.
    
    Khi phát hiện log ERROR có thể developer sẽ phải sửa ngay để đảm bảo ứng dụng tiếp tục hoạt động chính xác. Vậy bạn không nên lạm dụng log error để tránh nhận được các cảnh báo không cần thiết. Trong Java một số Exception được bạn tạo ra và đã được xử lý ví dụ khi login không tìm thấy User bạn throw ra UserNotFoundException trường hợp này cần cân nhắc để không ghi log Error nếu không bạn sẽ nhận được cảnh báo lỗi thường xuyên.

Ví dụ: Khách hàng thanh toán tiền cho dịch vụ A nhưng không thể trừ tiền của khách hàng do hệ thống thanh toán đang quá tải. Mỗi ERROR sẽ được ghi vào log-files để sau này có thể đối soát được lỗi quá tải đã gây ảnh hưởng đến bao nhiêu khách hàng.

FATAL/CRITICAL:
Log chỉ định để ghi các lỗi nghiêm trọng, thậm chí là đã gây ra gián đoạn dịch vụ, treo/crash phần mềm

`Thứ tự các level được mặc định như sau:
DEBUG --> INFO --> WARNING --> ERROR --> CRITICAL`

### Chúng ta cần ghi log những gì?

* Dữ liệu đầu vào và đầu ra
* Các bước xử lý trong các hàm, các service
* Thông tin liên quan khi thay đổi dữ liệu
* Thông tin liên quan tới perfomance
* Các rủi ro và lỗ hổng bảo mật
* Những dữ liệu không nên log hoặc phải ẩn thông tin trước khi ghi log
* Ghi log ngắn gọn nhất có thể

# `Một số thư viện hỗ trợ về việc bố trí ghi log`
Các thư viện mặc định được hỗ trợ trong java: `Java Util Logging`, `Log4J2`, `SLF4J` và `Logback`.

# `Cách cấu hình log level trên các môi trường dev, prod`

Việc set mức levels cho các môi trường như dev, staging, prod tùy thuộc vào yêu cầu chi tiết của từng môi trường:

dev: Khi phát triển luôn phải xử lý các trường thông thường và bất thường nên xét ở mức DEBUG cũng đủ để điều tra và xử lý.

stg: Khi đã phát triển xong cũng có lúc bị thiếu xót do vậy ở môi trường này cần mức DEBUG để điều tra các nguyên nhân gây lỗi trong hệ thống.

prod: Trước khi đến môi trường này đã qua hai môi trường dev và stg nên ở đây mức log có thể dùng INFO vì trong đó đã bao gồm WARN và EROROR, còn tùy thuộc vào yêu cầu có thể dùng mức WARN cho môi trường này.

`Cấu hình Log4j:`
![img_3.png](img_3.png)

rootLogger => cho show toàn bộ log cả ở console log và log được ghi trên file đối với môi trường trên dev hoặc stg

ConversionPattern => Định dạng log theo 1 pattern nhất định ở đây đang để định dạng
log4j.appender.* : cấu hình các file

Sử dụng:
![img_11.png](img_11.png)

Log trên console log
![img_10.png](img_10.png)

Log trên file
![img_12.png](img_12.png)

`Cấu hình Logback`

![img_6.png](img_6.png)

Pattern log:

    %d : thời gian ghi message, có thể chấp nhận tất các các định dạng SimperDateFormat cho phép

    %thread : tên thread ghi message

    $-5level : level ghi log (các mức level có thể là trace, debug, info, warn, error)

    %logger{36} : tên package class nơi log được ghi ra. Số 36 có ý nghĩa là lược ngắn tên package class trong trường hợp tên quá dài.

    %M : tên của method nơi log được ghi ra

    %msg: message chính

    %n : ngắt dòng

    %magenta() : đặt màu của message đầu ra trong dấu (), có thể chọn các màu khác

    highlight() : đặt màu của message đầu ra trong dấu (), tùy thuộc vào level log (ví dụ ERROR là màu đỏ)

Root level: "DEBUG" dành cho môi trường dev hoặc staging. Trên môi trường Prod nên để "ERROR" để giảm bớt số lượng log debug của các lập trình viên

Log trên console:
![img_8.png](img_8.png)

Log trên file
![img_7.png](img_7.png)

# `Các tool quản lý, monitor log:`
### `ELK Stack:`
ELK Stack là tập hợp 3 phần mềm đi chung với nhau, phục vụ cho công việc logging. Ba phần mềm này lần lượt là:

* Elasticsearch: Cơ sở dữ liệu để lưu trữ, tìm kiếm và query log
* Logstash: Tiếp nhận log từ nhiều nguồn, sau đó xử lý log và ghi dữ liệu và Elasticsearch
* Kibana: Giao diện để quản lý, thống kê log. Đọc thông tin từ Elasticsearch

![img_13.png](img_13.png)

Cơ chế hoạt động của ELK Stack

1. Đầu tiên, log sẽ được đưa đến Logstash. (Log từ hệ thống được gửi đến logstash)
2. Logstash sẽ đọc những log này, thêm những thông tin như thời gian, IP, parse dữ liệu từ log (server nào, độ nghiêm trọng, nội dung log) ra, sau đó ghi xuống database là Elasticsearch.
3. Khi muốn xem log, người dùng vào URL của Kibana. Kibana sẽ đọc thông tin log trong Elasticsearch, hiển thị lên giao diện cho người dùng query và xử lý. 

### Tại sao nên dùng ELK Stack?

*    Đọc log từ nhiều nguồn: Logstash có thể đọc được log từ rất nhiều nguồn, từ log file cho đến log database cho đến UDP hay REST request.
*    Dễ tích hợp: Dù bạn có dùng Nginx hay Apache, dùng MSSQL, MongoDB hay Redis, Logstash đều có thể đọc hiểu và xử lý log của bạn nên việc tích hợp rất dễ dàng.
*    Hoàn toàn free: Chỉ cần tải về, setup và dùng, không tốn một đồng nào cả. Công ty tạo ra ELK Stack kiếm tiền bằng các dịch vụ cloud hoặc các sản phẩm premium phụ thêm.
*    Khả năng scale tốt: Logstash và Elasticsearch chạy trên nhiều node nên hệ thống ELK cực kì dễ scale. Khi có thêm service, thêm người dùng, muốn log nhiều hơn, bạn chỉ việc thêm node cho Logstash và Elasticsearch là xong.
*    Search và filter mạnh mẽ: Elasticsearch cho phép lưu trữ thông tin kiểu NoSQL, hỗ trợ luôn Full-Text Search nên việc query rất dễ dàng và mạnh mẽ.
*    Cộng đồng mạnh, tutorial nhiều: Nhiều công ty dùng nên dĩ nhiên là có nhiều tutorial để học và dùng ELK Stack rồi.